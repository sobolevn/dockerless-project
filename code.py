import os

import psycopg2

print('Connecting to', os.environ['DATABASE_URL'])
connection = psycopg2.connect(os.environ['DATABASE_URL'])
assert connection
